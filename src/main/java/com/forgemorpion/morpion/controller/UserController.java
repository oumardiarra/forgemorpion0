package com.forgemorpion.morpion.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.forgemorpion.morpion.bean.User;
import com.forgemorpion.morpion.bussiness.user.UserBusiness;
import com.forgemorpion.morpion.dto.AuthRequest;
import com.forgemorpion.morpion.dto.RegisterRequest;
import com.forgemorpion.morpion.utils.AppUtils;

@RestController
@RequestMapping(UserController.PATH)
@CrossOrigin(origins = "*", allowedHeaders = "*") 
public class UserController {
	
	private UserBusiness userBusiness;
	
	static final String PATH = "users";

	public UserController(UserBusiness userBusiness) {
		super();
		this.userBusiness = userBusiness;
	}
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> authentication(@RequestBody AuthRequest authRequest) throws Exception {
		return ResponseEntity.ok(userBusiness.authentication(authRequest.getUsername(), authRequest.getPassword()));
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.CREATED)
	public User authentication(@RequestBody RegisterRequest registerRequest) throws Exception {
		User user = AppUtils.mapRegisterRequestToUser(registerRequest);
		System.out.println("controller auth " + user.toString());
		return userBusiness.registration(user);
	}
	
}
